# Fork this project
1. Fork this project to your personal gitlab account
1. Invite `henry@artistsworld.me`, `vic@artistsworld.me` as members of your forked project
1. Clone your forked project to your personal computer

Assume that you place this project at `D:\web` (You can put it at any directory of your choice)

# Development Preparation
1. Install [Docker Desktop](https://www.docker.com/products/docker-desktop)
1. [Register new account on Docker Hub](https://hub.docker.com/signup) and login to Docker Desktop app
1. Install [Visual Studio Code (VSC)](https://code.visualstudio.com/download)
1. Open VSC then `File > Open Folder` and browse to `D:\web\frontend`
1. Open Terminal (inside VSC) (Ctrl + `)
1. Run `docker-compose up -d` (accept sharing resources if prompts)
1. Run `docker exec -it vuejs sh`
1. Run `npm install -g @vue/cli`
1. Run `npm install`
1. run `npm run serve`

Open browser and browse to `http://127.0.0.1:8080` and confirm that the site is running properly.

# Development Notes
These below commands are run inside docker container.

You have to open `sh>` inside container before running them. 

To open `container sh`, run `docker exec -it vuejs sh`

> Start developing daily, run `npm run watch`
>
> To add vue module run `vue add <module-name>` (ex: `vue add vuex`)
>
> more later

# [Requirements](https://gitlab.com/aw-recruits/frontend/-/requirements_management/requirements?page=1&state=opened&sort=created_asc)
